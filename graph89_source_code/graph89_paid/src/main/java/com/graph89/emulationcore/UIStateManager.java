/*
 *   Graph89 - Emulator for Android
 *  
 *	 Copyright (C) 2012-2013  Dritan Hashorva
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.

 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.graph89.emulationcore;

import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;

import com.Bisha.TI89EmuDonation.R;
import com.graph89.common.CalculatorInstanceHelper;
import com.graph89.controls.ControlBar;
import com.graph89.controls.MessageView;

public class UIStateManager
{
	public ControlBar           ControlBarInstance          = null;
	public MessageView          MessageViewInstance         = null;
	public EmulatorView         EmulatorViewInstance        = null;
	public ActionsList          ActionsListInstance         = null;
	public DrawerLayout         mDrawerLayout               = null;
	public ButtonHighlightView	ButtonHighlightViewInstance	= null;
	private Context				mContext					= null;

	public UIStateManager(final Context context)
	{
		mContext = context;
		EmulatorActivity activity = (EmulatorActivity) mContext;

		ControlBarInstance = new ControlBar(context);

		MessageViewInstance = (MessageView) activity.findViewById(R.id.emulator_main_messageview);
		ActionsListInstance = (ActionsList) activity.findViewById(R.id.actionslist);
		EmulatorViewInstance = (EmulatorView) activity.findViewById(R.id.emulator_main_emulatorview);
		ButtonHighlightViewInstance = (ButtonHighlightView) activity.findViewById(R.id.emulator_main_buttonhighlightview);
		mDrawerLayout = (DrawerLayout) activity.findViewById(R.id.drawerLayout);

		ControlBarInstance.CalculatorTypeSpinnerInstance.setOnItemSelectedListener(new OnItemSelectedListenerWrapper(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
			{
				if (EmulatorActivity.ActiveInstance == null)
					return;
				EmulatorActivity.ActiveInstanceIndex = position;

				EmulatorActivity emulatorActivity = (EmulatorActivity) mContext;
				CalculatorInstanceHelper instances = new CalculatorInstanceHelper(context);
				instances.SetLastUsed(position);
				emulatorActivity.RestartEmulator();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0)
			{
			}
		}));
	}

	public void ShowTextViewer()
	{
		EmulatorViewInstance.setVisibility(View.GONE);
		MessageViewInstance.setVisibility(View.VISIBLE);
		ControlBarInstance.ShowControlBar();
		ControlBarInstance.HideCalculatorTypeSpinner();
	}

	public void ShowCalc()
	{
		ControlBarInstance.ShowCalculatorTypeSpinner();
		ControlBarInstance.HideControlBar();
		MessageViewInstance.setVisibility(View.GONE);
		EmulatorViewInstance.setVisibility(View.VISIBLE);
        ActionsListInstance.AdjustVisibility();
	}
}

class OnItemSelectedListenerWrapper implements OnItemSelectedListener
{
	private OnItemSelectedListener	listener;

	public OnItemSelectedListenerWrapper(OnItemSelectedListener aListener)
	{
		listener = aListener;
	}

	@Override
	public void onItemSelected(AdapterView<?> aParentView, View aView, int aPosition, long anId)
	{
		if (EmulatorActivity.ActiveInstanceIndex != aPosition)
		{
			listener.onItemSelected(aParentView, aView, aPosition, anId);
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> aParentView)
	{
		listener.onNothingSelected(aParentView);
	}
}
